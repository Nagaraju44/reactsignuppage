import React, { Component } from 'react'
import './index.css'

export class SuccessMsg extends Component {
    constructor(props){
        super(props)
    }
  render() {
    return (
      <div className='success-container'>
        <h1 className='success-heading'>Congratulations {this.props.FirstName} {this.props.LastName}. You have SuccessFully Created Account</h1>
        <p className='update-intial'>We will send Email If any Updates are there</p>
      </div>
    )
  }
}

export default SuccessMsg