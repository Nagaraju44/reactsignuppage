import React, { Component } from 'react';
import './index.css'

class Header extends Component{
    render(){
        return (<>
        <h1 className='heading'>Welcome</h1>
        <p className='join-community'>Join the World's Largest Community</p>
        <h3 className='signup-here'>
            Signup Here
        </h3>
        
        </>)
    }
}

export default Header