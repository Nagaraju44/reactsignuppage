import React, { Component } from "react";
import validator from 'validator';
import SuccessMsg from '../Success';
import './index.css'


class Form extends Component {
    state = {
        FirstName: "",
        LastName: "",
        Age: 0,
        Gender: "",
        Role: "",
        Email: "",
        Password: "",
        reEnterPassword: "",
        Error: {},
        Success: "",
        FirstNameValid: false,
        LastNameValid: false,
        AgeValid: false,
        isGenderSelected: false,
        isRoleSelected: false,
        EmailValid: false,
        PasswordValid: false,
        RePasswordValid: false,
        isPasswordMatched: false,
        isChecked: false,
        isSubmited: false
    }

    FirstNameInputValidation = (event) => {
        if (validator.isAlpha(event.target.value) === true) {
            this.setState({ FirstName: event.target.value, FirstNameValid: true })
            this.setState((prevState) => {
                { prevState.Error.FirstName = "" }
            })
        } else {
            this.setState({ FirstName: "", FirstNameValid: false })
            this.setState((prevState) => {
                { prevState.Error.FirstName = "*Please Enter Valid FirstName" }
            })
        }
    }

    LastNameInputValidation = (event) => {
        if (validator.isAlpha(event.target.value) === true) {
            this.setState({ LastName: event.target.value, LastNameValid: true })
            this.setState((prevState) => {
                { prevState.Error.LastName = "" }
            })
        } else {
            this.setState({ LastName: "", LastNameValid: false })
            this.setState((prevState) => {
                { prevState.Error.LastName = "*Please Enter Valid LastName" }
            })
        }
    }

    AgeValidation = (event) => {
        if (validator.isInt(event.target.value, { min: 0, max: 100 }) === true) {
            this.setState({ Age: event.target.value, AgeValid: true })
            this.setState((prevState) => {
                { prevState.Error.Age = "" }
            })
        } else {
            this.setState({ Age: event.target.value, AgeValid: false })
            this.setState((prevState) => {
                { prevState.Error.Age = "*Age Must be in Numbers b/w 0 to 100" }
            })
        }
    }

    GenderValidation = (event) => {
        if (event.target.value !== "---Select---") {
            this.setState({ Gender: event.target.value, isGenderSelected: true })
            this.setState((prevState) => {
                { prevState.Error.Gender = "" }
            })
        } else {
            this.setState({ Gender: event.target.value, isGenderSelected: false })
            this.setState((prevState) => {
                { prevState.Error.Gender = "*Please Select Gender" }
            })
        }
    }

    RoleValidation = (event) => {
        if (event.target.value !== "---Select---") {
            this.setState({ Role: event.target.value, isRoleSelected: true })
            this.setState((prevState) => {
                { prevState.Error.Role = "" }
            })
        } else {
            this.setState({ Role: event.target.value, isRoleSelected: false })
            this.setState((prevState) => {
                { prevState.Error.Role = "*Please Select Role" }
            })
        }
    }

    EmailValidation = (event) => {
        if (validator.isEmail(event.target.value) === true) {
            this.setState({ Email: event.target.value, EmailValid: true })
            this.setState((prevState) => {
                { prevState.Error.Email = "" }
            })
        } else {
            this.setState({ Email: event.target.value, EmailValid: false })
            this.setState((prevState) => {
                { prevState.Error.Email = "*Please Enter Valid Email" }
            })
        }
    }

    PasswordValidation = (event) => {
        if (validator.isStrongPassword(event.target.value) === true) {
            this.setState({ Password: event.target.value, PasswordValid: true })
            this.setState((prevState) => {
                { prevState.Error.Password = "" }
            })
        } else {
            this.setState({ Password: event.target.value, PasswordValid: false })
            this.setState((prevState) => {
                { prevState.Error.Password = "*Password must contain 1 uppercase,1 lowecase,1 numeric,1 special char, min 8 chars" }
            })
        }
    }

    RepeatPasswordValidation = (event) => {
        const { Password } = this.state
        if (validator.isStrongPassword(event.target.value) === true) {
            this.setState({ reEnterPassword: event.target.value, RePasswordValid: true })
            this.setState((prevState) => {
                { prevState.Error.reEnterPassword = "" }
            })
        } else {
            this.setState({ reEnterPassword: event.target.value, RePasswordValid: false })
            this.setState((prevState) => {
                { prevState.Error.reEnterPassword = "*Password must contain 1 uppercase,1 lowecase,1 numeric,1 special char, min 8 chars" }
            })
        }

        if (Password !== event.target.value) {
            this.setState({ isPasswordMatched: false })
            this.setState((prevState) => {
                { prevState.Error.reEnterPassword = "*Password must be equal in both entries" }
            })
        } else {
            this.setState({ isPasswordMatched: true })
            this.setState((prevState) => {
                { prevState.Error.reEnterPassword = "" }
            })
        }

    }

    AgreeValidation = (event) => {
        const { isChecked } = this.state
        if (isChecked === false) {
            this.setState({ isChecked: true })
            this.setState((prevState) => {
                { prevState.Error.isChecked = "" }
            })
        } else {
            this.setState({ isChecked: false })
        }
    }

    SubmitValidation = (event) => {
        event.preventDefault()
        console.log(event);
        const { FirstNameValid, LastNameValid, AgeValid, isGenderSelected, isRoleSelected, EmailValid, PasswordValid,
            RePasswordValid, isPasswordMatched, isChecked } = this.state
        if (FirstNameValid === false) {
            this.setState({ FirstName: "", FirstNameValid: false })
            this.setState((prevState) => {
                { prevState.Error.FirstName = "*Please Enter Valid FirstName" }
            })
        }
        if (LastNameValid === false) {
            this.setState({ LastName: "", LastNameValid: false })
            this.setState((prevState) => {
                { prevState.Error.LastName = "*Please Enter Valid LastName" }
            })
        }
        if (AgeValid === false) {
            this.setState({ Age: event.target.value, AgeValid: false })
            this.setState((prevState) => {
                { prevState.Error.Age = "*Age Must be in Numbers b/w 0 to 100" }
            })
        }
        if (isGenderSelected === false) {
            this.setState({ Gender: event.target.value, isGenderSelected: false })
            this.setState((prevState) => {
                { prevState.Error.Gender = "*Please Select Gender" }
            })
        }
        if (isRoleSelected === false) {
            this.setState({ Role: event.target.value, isRoleSelected: false })
            this.setState((prevState) => {
                { prevState.Error.Role = "*Please Select Role" }
            })
        }
        if (EmailValid === false) {
            this.setState({ Email: event.target.value, EmailValid: false })
            this.setState((prevState) => {
                { prevState.Error.Email = "*Please Enter Valid Email" }
            })
        }
        if (PasswordValid === false) {
            this.setState({ Password: event.target.value, PasswordValid: false })
            this.setState((prevState) => {
                { prevState.Error.Password = "*Password must contain 1 uppercase,1 lowecase,1 numeric,1 special char, min 8 chars" }
            })
        }
        if (RePasswordValid === false) {
            this.setState({ reEnterPassword: event.target.value, RePasswordValid: false })
            this.setState((prevState) => {
                { prevState.Error.reEnterPassword = "*Password must contain 1 uppercase,1 lowecase,1 numeric,1 special char, min 8 chars" }
            })
        }
        if (isChecked === false) {
            this.setState({ isChecked: false })
            this.setState((prevState) => {
                { prevState.Error.isChecked = "*Please Agree to the terms" }
            })
        }
        if (FirstNameValid && LastNameValid && AgeValid &&
            isGenderSelected && isRoleSelected && EmailValid && PasswordValid &&
            RePasswordValid && isPasswordMatched && isChecked) {
            this.setState({ Success: "SuccessFully Registered Thank you", isSubmited: true })
            this.setState((prevState) => {
                { prevState.Error.isChecked = "" }
            })
        }


    }

    render() {
        const { Error, Success, isSubmited, FirstName, LastName } = this.state
        console.log(this.state);
        if (isSubmited) {
            return <SuccessMsg FirstName={FirstName} LastName={LastName} />
        } else {
            return (
                <div className="Form-container">
                    <form className="form" onSubmit={this.SubmitValidation}>
                        <div className="input-container">
                            <label className="label">FirstName</label><br />
                            <input type="text" placeholder="Enter FirstName..." id="FirstName" className="Input-text" onBlur={this.FirstNameInputValidation} /><br />
                            <span className="Error">{Error["FirstName"]}</span>
                        </div>
                        <div className="input-container">
                            <label className="label">LastName</label><br />
                            <input type="text" placeholder="Enter LastName..." id="LastName" className="Input-text" onBlur={this.LastNameInputValidation} /><br />
                            <span className="Error">{Error["LastName"]}</span>
                        </div>
                        <div className="input-container">
                            <label className="label">Age</label><br />
                            <input type="text" placeholder="Enter Your Age..." id="Age" className="Input-text" onBlur={this.AgeValidation} /><br />
                            <span className="Error">{Error["Age"]}</span>
                        </div>
                        <div className="input-container">
                            <label className="label">Gender</label><br />
                            <select className="dropdown" onBlur={this.GenderValidation}>
                                <option>---Select---</option>
                                <option>Male</option>
                                <option>Female</option>
                                <option>Other</option>
                            </select><br />
                            <span className="Error">{Error["Gender"]}</span>
                        </div>
                        <div className="input-container">
                            <label className="label">Role</label><br />
                            <select className="dropdown" onBlur={this.RoleValidation}>
                                <option>---Select---</option>
                                <option>Developer</option>
                                <option>Senior Developer</option>
                                <option>Lead Engineer</option>
                                <option>CTO</option>
                                <option>Other</option>
                            </select><br />
                            <span className="Error">{Error["Role"]}</span>
                        </div>
                        <div className="input-container">
                            <label className="label">Email</label><br />
                            <input type="text" placeholder="Enter Your Email..." id="Email" className="Input-text" onBlur={this.EmailValidation} /><br />
                            <span className="Error">{Error["Email"]}</span>
                        </div>
                        <div className="input-container">
                            <label className="label">Password</label><br />
                            <input type="Password" placeholder="Enter Password..." className="Input-text" onBlur={this.PasswordValidation} /><br />
                            <span className="Error">{Error["Password"]}</span>
                        </div>
                        <div className="input-container">
                            <label className="label">Repeat Password</label><br />
                            <input type="Password" placeholder="Repeat Password..." className="Input-text" onBlur={this.RepeatPasswordValidation} /><br />
                            <span className="Error">
                                {Error["reEnterPassword"]}
                            </span>
                        </div>
                        <div className="terms-container">
                            <input type="checkbox" className="checkbox" onClick={this.AgreeValidation} />
                            <label><a href="">Agree to Terms&Condition</a></label><br />
                            <span className="Error">{Error["isChecked"]}</span>
                        </div>
                        <div className="btn-container">
                            <button className="signup-btn" type="Submit">SignUp</button>
                        </div>
                        <span className="Success">{Success}</span>
                    </form>
                </div>
            )
        }
    }
}

export default Form